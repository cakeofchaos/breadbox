$("#tbl_year_sales tbody tr").click(function () {
    let year = $(this).data("year");
    let month = $(this).data("month");
    if (year && month) {
        getMonthData(year, month);
        scrollToElement('#tbl_month_sales');
    }
});

getMonthData = function (year, month) {
    $("#tbl_month_sales tbody tr").remove();
    $.getJSON("http://localhost:8080/api/statistics/sales/" + year +"/" + month, function (data) {
        $.each( data, function (key, val) {
            row = $('<tr>')
                .attr('data-year', year)
                .attr('data-month', month)
                .attr('data-day', key);
            row.append($('<td>').text(key));
            $.each(val, function (key, val) {
                row.append($('<td>').text(val));
            });
            row.appendTo('#tbl_month_sales');
        })
    });
};

$("#tbl_month_sales tbody").on('click', 'tr', function () {
    let year = $(this).data("year");
    let month = $(this).data("month");
    let day = $(this).data("day");
    if (year && month && day) {
        getDayData(year, month, day);
        scrollToElement('#tbl_day_sales');
    }
});

getDayData = function (year, month, day) {
    $("#tbl_day_sales tbody tr").remove();
    $.getJSON("http://localhost:8080/api/statistics/sales/" + year +"/" + month + "/" + day, function (data) {
        $.each( data, function (key, val) {
            row = $('<tr>')
                .attr('data-year', year)
                .attr('data-month', month)
                .attr('data-day', day)
                .attr('data-hour', key);
            row.append($('<td>').text(key));
            $.each(val, function (key, val) {
                row.append($('<td>').text(val));
            });
            row.appendTo('#tbl_day_sales');
        })
    });
};

scrollToElement = function (element) {
    var new_position = $(element).offset();
    $('html, body').stop().animate({ scrollTop: new_position.top }, 500);
}