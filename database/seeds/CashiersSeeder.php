<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CashiersSeeder extends Seeder
{
    protected $cashiers = [
        1 => "-8",
        2 => "0",
        3 => "+8",
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->cashiers as $id => $timezone) {
            DB::table('cashiers')->insert([
                'id' => $id,
                'timezone' => $timezone,
            ]);
        }
    }
}
