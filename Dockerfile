FROM php:7.3-fpm

LABEL maintainer="Merzlyakov Dmitriy"

RUN apt-get update && apt-get install -y \
        curl \
        wget \
        git \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libzip-dev
RUN docker-php-ext-install -j$(nproc) iconv mbstring mysqli pdo_mysql zip calendar exif bcmath sockets
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd

RUN apt-get install -y --no-install-recommends libsodium-dev

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN apt-get update && apt-get install -y \
    libmagickwand-dev --no-install-recommends \
    && pecl install imagick \
	&& docker-php-ext-enable imagick

WORKDIR /var/www

COPY bootstrap bootstrap
COPY config config
COPY app app
COPY database database
COPY public public
COPY resources resources
COPY routes routes
COPY tests tests
COPY storage storage

COPY composer.json composer.json
COPY package.json package.json
COPY readme.md readme.md
COPY artisan artisan
COPY server.php server.php
COPY .env.presentation .env

RUN mkdir -p storage
RUN mkdir -p storage/logs
RUN mkdir -p storage/framework
RUN mkdir -p storage/framework/cache
RUN mkdir -p storage/framework/sessions
RUN mkdir -p storage/framework/views
RUN composer install
RUN chown -R www-data:www-data /var/www
RUN chmod -R 777 /var/www

RUN apt-get install -y supervisor
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

ADD start.sh /
RUN chmod +x /start.sh
ENTRYPOINT ["/start.sh"]
