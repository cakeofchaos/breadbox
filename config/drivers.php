<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Receipts Consumer Driver
    |--------------------------------------------------------------------------
    |
    | Recipts Consumer support different consumer drivers with a single api. Here you
    | can choose the concrete consumer driver.
    |
    | Supported: "faker", "rmq"
    |
    */
    'receipts_consumer' => env('RECEIPTS_CONSUMER_DRIVER', 'faker'),
];