<?php

namespace App\Http\Controllers\Api\Statistics;

use App\Http\Controllers\Controller;
use App\Services\Statistics\SalesStatisticsService;

class SalesController extends Controller
{
    public function index($year, SalesStatisticsService $salesStatisticsService)
    {
        $salesByMonths = $salesStatisticsService->getMonthsStatisticsThisYear($year, 3);

        return response()->json($salesByMonths);
    }

    public function month($year, $month, SalesStatisticsService $salesStatisticsService)
    {
        $salesByDays = $salesStatisticsService->getDaysStatisticsThisMonth($year, $month, 3);

        return response()->json($salesByDays);
    }

    public function day($year, $month, $day, SalesStatisticsService $salesStatisticsService)
    {
        $salesByHours = $salesStatisticsService->getHoursStatisticsThisDay($year, $month, $day, 3);

        return response()->json($salesByHours);
    }
}
