<?php
/**
 * Created by PhpStorm.
 * User: cakeofchaos
 * Date: 21.10.19
 * Time: 19:23
 */

namespace App\Http\Controllers;


class HomeController extends Controller
{
    public function index()
    {
        return view('welcome');
    }
}