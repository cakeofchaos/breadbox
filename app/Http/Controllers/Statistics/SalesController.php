<?php

namespace App\Http\Controllers\Statistics;

use App\Http\Controllers\Controller;
use App\Models\Cashier;
use App\Services\Statistics\SalesStatisticsService;

class SalesController extends Controller
{
    public function index($year, SalesStatisticsService $salesStatisticsService)
    {
        $cashiers = Cashier::all();

        $salesByMonths = $salesStatisticsService->getMonthsStatisticsThisYear($year, 3);

        return view('statistics.sales.index', [
            'salesByMonths' => $salesByMonths,
            'cashiers' => $cashiers,
            'year' => $year
        ]);
    }
}
