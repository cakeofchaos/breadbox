<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cashier extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'id',
        'timezone'
    ];

    public function receipts()
    {
        return $this->hasMany(Receipt::class);
    }
}
