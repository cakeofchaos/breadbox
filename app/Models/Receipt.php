<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'id',
        'cashier_id',
        'date',
        'sum'
    ];

    public function cashier()
    {
        return $this->belongsTo(Cashier::class);
    }
}
