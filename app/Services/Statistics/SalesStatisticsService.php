<?php
/**
 * Created by PhpStorm.
 * User: cakeofchaos
 * Date: 21.10.19
 * Time: 15:18
 */

namespace App\Services\Statistics;


use App\Models\Cashier;
use App\Models\Receipt;
use Illuminate\Support\Facades\DB;

class SalesStatisticsService
{
    public function getMonthsStatisticsThisYear($year, $timeZone = 0)
    {
        $subQuery = $this->getTimeZonedSubQuery($timeZone);
        $salesByMonths = DB::query()->fromSub($subQuery, 'tz_cashiers')
            ->selectRaw('tz_date, cashier_id, SUM(sum) as sum')
            ->selectRaw('MONTH(tz_date) as month')
            ->whereYear('tz_date', '=', $year)
            ->groupBy('month')
            ->groupBy('cashier_id')
            ->get();

        return $this->mapStatisticsData($salesByMonths, 'month');
    }

    public function getDaysStatisticsThisMonth($year, $month, $timeZone = 0)
    {
        $subQuery = $this->getTimeZonedSubQuery($timeZone);
        $salesByDays = DB::query()->fromSub($subQuery, 'tz_cashiers')
            ->selectRaw('tz_date, day(tz_date) as day, cashier_id, SUM(sum) as sum')
            ->whereYear('tz_date', '=', $year)
            ->whereMonth('tz_date', '=', $month)
            ->groupBy('day')
            ->groupBy('cashier_id')
            ->get();

        return $this->mapStatisticsData($salesByDays, 'day');
    }

    public function getHoursStatisticsThisDay($year, $month, $day, $timeZone = 0)
    {
        $subQuery = $this->getTimeZonedSubQuery($timeZone);
        $salesByHours = DB::query()->fromSub($subQuery, 'tz_cashiers')
            ->selectRaw('hour(tz_date) as hour')
            ->selectRaw('cashier_id')
            ->selectRaw('SUM(sum) as sum')
            ->whereYear('tz_date', '=', $year)
            ->whereMonth('tz_date', '=', $month)
            ->whereDay('tz_date', '=', $day)
            ->groupBy('cashier_id')
            ->groupBy('hour')
            ->get();

        return $this->mapStatisticsData($salesByHours, 'hour');
    }



    protected function mapStatisticsData($results, $periodFieldName)
    {
        $cashiers = Cashier::all();
        $salesStatistics = $results->groupBy($periodFieldName)
            ->map(function ($month) use ($cashiers) {
                $salesByCashiers = $month->keyBy('cashier_id');
                return $cashiers->mapWithKeys(function ($item, $key) use ($salesByCashiers) {
                    $cashierId = $item['id'];
                    $salesInfo = $salesByCashiers->get($cashierId);
                    return [$cashierId => $salesInfo?$salesInfo->sum:null];
                });
            });

        return $salesStatistics;
    }

    protected function getTimeZonedSubQuery($timeZone)
    {
        $subQuery = Receipt::join('cashiers', 'cashiers.id', '=', 'receipts.cashier_id')
            ->selectRaw('receipts.*')
            ->selectRaw("DATE_ADD(date, INTERVAL timezone + $timeZone hour) as tz_date");

        return $subQuery;
    }
}