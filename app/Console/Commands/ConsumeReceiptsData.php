<?php

namespace App\Console\Commands;

use App\Console\Services\ReceiptsDataConsumers\ReceiptsDataConsumer;
use Illuminate\Console\Command;

/**
 * Class ConsumeReceiptsData
 * @package App\Console\Commands
 */
class ConsumeReceiptsData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:consume-receipts-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comsume receipts data.';

    /**
     * Execute the console command.
     *
     * @param ReceiptsDataConsumer $consumer
     * @return mixed
     */
    public function handle(ReceiptsDataConsumer $consumer)
    {
        $consumer->consume();
    }
}
