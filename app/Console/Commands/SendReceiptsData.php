<?php

namespace App\Console\Commands;

use App\Console\Services\ReceiptsDataGenerator;
use Carbon\Carbon;
use Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Class SendReceiptsData
 * @package App\Console\Commands
 */
class SendReceiptsData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:send-receipts-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate and send receipts data';

    /**
     * Execute the console command.
     *
     * @param AMQPStreamConnection $connection
     * @param ReceiptsDataGenerator $receiptsDataGenerator
     * @return mixed
     */
    public function handle(
        AMQPStreamConnection $connection,
        ReceiptsDataGenerator $receiptsDataGenerator
    ) {
        echo 'test';
        $channel = $connection->channel();
        $channel->queue_declare(config('rmq.queues.receipts'), false, true, false, false);

        $startPeriod = $this->getStartHour();

        for ($i = 0; $i < 365; $i++) {
            $endPeriod = $startPeriod->copy()->setHour(22);
            for ($receiptNumber = 0; $receiptNumber < rand(400, 800); $receiptNumber++) {
                $receiptData = $receiptsDataGenerator->generateReceiptData(
                    $startPeriod->toDateTimeString(),
                    $endPeriod->toDateTimeString()
                );
                $message = new AMQPMessage(
                    json_encode($receiptData),
                    [
                        'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
                    ]
                );
                $channel->basic_publish($message, '', config('rmq.queues.receipts'));
            }
            $startPeriod = $startPeriod->addDay();
        }
    }

    /**
     * @return \Carbon\CarbonInterface
     */
    protected function getStartHour()
    {
        return Carbon::now()
            ->setHour(6)
            ->setMinute(0)
            ->setSecond(0)
            ->subYear(1);
    }
}
