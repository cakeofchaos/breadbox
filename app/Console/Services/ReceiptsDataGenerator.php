<?php
/**
 * Created by PhpStorm.
 * User: cakeofchaos
 * Date: 21.10.19
 * Time: 13:50
 */

namespace App\Console\Services;


use Faker\Generator;

class ReceiptsDataGenerator
{
    /**
     * @var Generator
     */
    protected $faker;

    /**
     * Create a new command instance.
     *
     * @param Generator $faker
     */
    public function __construct(Generator $faker)
    {
        $this->faker = $faker;
    }

    public function generateReceiptData($startPeriod = null, $endPeriod = null)
    {
        if ($startPeriod && $endPeriod) {
            $date = $this->faker->dateTimeBetween($startPeriod, $endPeriod);
        } else {
            $date = $this->faker->dateTimeThisYear();
        }
        echo $date->format("Y-m-d H:i:s") . PHP_EOL;
        return [
            'id' => $this->faker->unique()->randomNumber(8),
            'cachier_id' => $this->faker->numberBetween(1, 3),
            'date' => $date->format("Y-m-d H:i:s"),
            'sum' => $this->faker->randomFloat(2, 80, 150),
            $this->faker->dateTimeBetween()
        ];
    }
}