<?php
/**
 * Created by PhpStorm.
 * User: cakeofchaos
 * Date: 21.10.19
 * Time: 13:33
 */

namespace App\Console\Services;

use App\Console\Services\ReceiptsDataConsumers\FakerReceiptsDataConsumer;
use App\Console\Services\ReceiptsDataConsumers\RmqReceiptsDataConsumer;
use Faker\Generator;
use Illuminate\Support\Manager;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class ReceiptsDataConsumersManager extends Manager
{

    /**
     * Get the default driver name.
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        return app()['config']['drivers.receipts_consumer'];
    }

    protected function createFakerDriver()
    {
        $faker = resolve(Generator::class);
        return new FakerReceiptsDataConsumer($faker);
    }

    protected function createRmqDriver()
    {
        $connection = resolve(AMQPStreamConnection::class);
        return new RmqReceiptsDataConsumer($connection);
    }
}