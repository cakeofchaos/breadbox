<?php
/**
 * Created by PhpStorm.
 * User: cakeofchaos
 * Date: 21.10.19
 * Time: 13:34
 */

namespace App\Console\Services\ReceiptsDataConsumers;


interface ReceiptsDataConsumer
{
    public function consume();
}