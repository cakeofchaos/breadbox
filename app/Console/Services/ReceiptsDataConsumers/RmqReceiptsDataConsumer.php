<?php
/**
 * Created by PhpStorm.
 * User: cakeofchaos
 * Date: 21.10.19
 * Time: 13:36
 */

namespace App\Console\Services\ReceiptsDataConsumers;


use App\Models\Receipt;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;

/**
 * Class RmqReceiptsDataConsumer
 * @package App\Console\Services\ReceiptsDataConsumers
 */
class RmqReceiptsDataConsumer implements ReceiptsDataConsumer
{
    /**
     * @var AMQPStreamConnection
     */
    protected $connection;

    /**
     * @var AMQPChannel
     */
    protected $channel;

    /**
     * RmqReceiptsDataConsumer constructor.
     * @param $connection
     */
    public function __construct(AMQPStreamConnection $connection)
    {
        $this->connection = $connection;
        $this->channel = $connection->channel();
        $this->channel->queue_declare(config('rmq.queues.receipts'), false, true, false, false);
    }

    /**
     *
     */
    public function consume()
    {
        $this->channel->basic_qos(null, 1, null);
        $this->channel->basic_consume(
            config('rmq.queues.receipts'),
            '',
            false,
            true,
            false,
            false,
            function ($message) {
                echo $message->body . PHP_EOL;
                $receiptData = json_decode($message->body);
                $this->saveReceiptData($receiptData);

            }
        );

        while ($this->channel->is_consuming()) {
            $this->channel->wait();
        }
    }

    /**
     * @param $receiptData
     */
    protected function saveReceiptData($receiptData)
    {
        Receipt::create([
            'id' => $receiptData->id,
            'cashier_id' => $receiptData->cachier_id,
            'date' => $receiptData->date,
            'sum' => $receiptData->sum,
        ]);
    }
}