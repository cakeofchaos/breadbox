<?php
/**
 * Created by PhpStorm.
 * User: cakeofchaos
 * Date: 21.10.19
 * Time: 13:36
 */

namespace App\Console\Services\ReceiptsDataConsumers;


use App\Console\Services\ReceiptsDataGenerator;
use App\Models\Receipt;

/**
 * Class FakerReceiptsDataConsumer
 * @package App\Console\Services\ReceiptsDataConsumers
 */
class FakerReceiptsDataConsumer implements ReceiptsDataConsumer
{
    /**
     * @var ReceiptsDataGenerator
     */
    protected $receiptsDataGenerator;

    /**
     * FakerReceiptsDataConsumer constructor.
     * @param $receiptsDataGenerator
     */
    public function __construct(ReceiptsDataGenerator $receiptsDataGenerator)
    {
        $this->receiptsDataGenerator = $receiptsDataGenerator;
    }


    /**
     *
     */
    public function consume()
    {
        while (true) {
            $receiptData = $this->receiptsDataGenerator->generateReceiptData();
            $this->saveReceiptData($receiptData);
            sleep(5);
        }
    }

    /**
     * @param $receiptData
     */
    protected function saveReceiptData($receiptData)
    {
        Receipt::create([
            'id' => $receiptData['id'],
            'cashier_id' => $receiptData['cachier_id'],
            'date' => $receiptData['date'],
            'sum' => $receiptData['sum'],
        ]);
    }
}