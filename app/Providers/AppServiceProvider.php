<?php

namespace App\Providers;

use App\Console\Services\ReceiptsDataConsumers\ReceiptsDataConsumer;
use App\Console\Services\ReceiptsDataConsumersManager;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(ReceiptsDataConsumer::class, function ($app) {
            $manager = new ReceiptsDataConsumersManager($app);

            return $manager->driver();
        });
    }
}
