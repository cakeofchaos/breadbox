<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
</head>
<body>
    <table id="tbl_year_sales" border="1" width="100%">
        <thead>
            <tr>
                <th></th>
                @foreach($cashiers as $cashier)
                    <td>
                        Касса {{ $cashier->id }}
                    </td>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($salesByMonths as $month => $salesInMonth)
                <tr data-year="{{ $year }}" data-month="{{ $month }}">
                    <td>{{ $month }}</td>
                    @foreach($cashiers as $cashier)
                        @if( $salesInMonth->has($cashier->id))
                            <td>{{ $salesInMonth->get($cashier->id) }}</td>
                        @else
                            <td>-</td>
                        @endif
                    @endforeach
                </tr>
            @endforeach
        </tbody>
    </table>
    <table id="tbl_month_sales" border="1" width="100%">
        <thead>
            <tr>
                <th></th>
                @foreach($cashiers as $cashier)
                    <td>
                        Касса {{ $cashier->id }}
                    </td>
                @endforeach
            </tr>
        </thead>
        <tbody></tbody>
    </table>
    <table id="tbl_day_sales" border="1" width="100%">
        <thead>
            <tr>
                <th></th>
                @foreach($cashiers as $cashier)
                    <td>
                        Касса {{ $cashier->id }}
                    </td>
                @endforeach
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
<script type="text/javascript" src="{{ asset('/js/sales-statistics.js') }}"></script>
</html>
